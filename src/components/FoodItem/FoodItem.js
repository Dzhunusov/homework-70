import React from 'react';
import './FoodItem.css';

const FoodItem = props => {
  return (
      <div className="food-item">
        Name : {props.name}
        Price : {props.price}
        {props.image}
      </div>
  );
};

export default FoodItem;