import React, {useEffect} from 'react';
import './Menu.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchMenu} from "../../store/actions";

const Menu = () => {
  const foods = useSelector(state => state.foods);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMenu());
    console.log(foods);
  }, [dispatch]);

  // const foodsInfo = () => {
  //   foods.map(num => (
  //       <FoodItem
  //           name: {num.name}
  //           price: {num.price}
  //           image: {num.image}
  //       />
  //   ))
  // }


  return (
      <div className="menu">
        MENU
      </div>
  );
};

export default Menu;