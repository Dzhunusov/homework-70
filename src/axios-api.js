import axios from "axios";

const axiosApi = axios.create({
  baseURL: 'https://burgerjs7.firebaseio.com',
});

export default axiosApi;