import {FETCH_MENU_ERROR, FETCH_MENU_REQUEST, FETCH_MENU_SUCCESS} from "./actionTypes";
import axiosApi from "../axios-api";
import React from "react";

const fetchMenuRequest = () => {
  return {type: FETCH_MENU_REQUEST}
};

const fetchMenuSuccess = value => {
  return {type: FETCH_MENU_SUCCESS, value}
};

const fetchMenuError = error => {
  return {type: FETCH_MENU_ERROR, error}
};

export const fetchMenu = () => {
  return async dispatch => {
    dispatch(fetchMenuRequest());
    try{
      const response = await axiosApi.get("/foods.json");
      // console.log(response.data);
      dispatch(fetchMenuSuccess(response.data));
    } catch (e) {
      dispatch(fetchMenuError(e));
    }
  }
}