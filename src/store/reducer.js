import {FETCH_MENU_ERROR, FETCH_MENU_REQUEST, FETCH_MENU_SUCCESS} from "./actionTypes";

const initialState = {
  foods: [],
  loading: false,
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MENU_REQUEST:
      return {...state, loading: true};
    case FETCH_MENU_SUCCESS:
      console.log(action.value);
      return {...state, loading: false, foods: action.value};
    case FETCH_MENU_ERROR:
      return {...state, loading: false, error: action.error}
    default:
      return state;
  }
}

export default reducer;